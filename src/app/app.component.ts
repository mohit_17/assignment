import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { MapsAPILoader, MouseEvent } from '@agm/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import {$,jQuery} from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  imageChangedEvent: any = '';
  croppedImage: any = '';

  title: string = 'AGM project';
  latitude: number;
  longitude: number;
  zoom: number;
  address: string;
  private geoCoder; 

  @ViewChild('search')
  public searchElementRef: ElementRef;
  
  fileChangeEvent(event: any): void {
      this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
      this.croppedImage = event.base64;
  }
  imageLoaded() {
      // show cropper
  }
  cropperReady() {
      // cropper ready
  }
  loadImageFailed() {
      // show message
  }

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) { }


  ngOnInit() {


    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }

  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }

  cstCCNumber;
  cc_format(ccid,ctid) {
  // supports Amex, Master Card, Visa, and Discover
  // parameter 1 ccid= id of credit card number field
  // parameter 2 ctid= id of credit card type field

  var ccNumString = this.cstCCNumber;
  // console.log(ccNumString , "ccNumString ccNumString")
  ccNumString=ccNumString.replace(/[^0-9]/g, '');
  console.log(ccNumString , "ccNumString")
  // mc, starts with - 51 to 55
  // v, starts with - 4
  // dsc, starts with 6011, 622126-622925, 644-649, 65
  // amex, starts with 34 or 37
  var typeCheck = ccNumString.substring(0, 2);

  console.log(typeCheck.length , "typeCheck")

  if  (typeCheck.length==2) {
    console.log("ifee")
      typeCheck=parseInt(typeCheck);
      console.log(typeCheck , "typeCheck")
      if (typeCheck >= 40 && typeCheck <= 49) {
          console.log("Its a Visa Card");
          alert("Its a Visa Card");
      }
      else if (typeCheck >= 51 && typeCheck <= 55) {
        console.log("Its a Master Card");
        alert("Its a Master Card");
      }
      else if ((typeCheck >= 60 && typeCheck <= 62) || (typeCheck == 64) || (typeCheck == 65)) {
        console.log("Its a Discover Card");
        alert("Its a Discover Card");
      }
      else if (typeCheck==34 || typeCheck==37) {
        console.log("Its a American Express Card");
        alert("Its a American Express Card");
      }
      else {
        console.log("Your Card is invalid");
      }
  }

 
}



  markerDragEnd($event: MouseEvent) {
    console.log($event);
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }
  

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  }
 
}
